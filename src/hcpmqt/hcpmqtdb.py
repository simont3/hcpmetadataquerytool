# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>


import time
import os, sys
import sqlite3
import threading
from hcpmqt.hcpmqtinit import gVars

class dbHandler(threading.Thread):
    'Setup and write a SQLite3 database file'
    
    def __init__(self, filename, rcvQueue):
        'Open the database file and initialise the db schema'
        threading.Thread.__init__(self)
        self.filename = filename
        self.rcvQueue = rcvQueue
        # delete database file if existant
        try:
            os.remove(filename)
        except (OSError) as e:
            pass
        
        ## setup database
        try:
            self.dbConn = sqlite3.connect(self.filename)
            self.dbConn.isolation_level = None # use AUTOCOMMIT mode
            self.ic = self.dbConn.cursor()
        except sqlite3.DatabaseError as e:
            raise(e)

        dbSchema = {"admin": '''CREATE TABLE admin
                                (magic           TEXT,
                                 version         TEXT,
                                 creation        TEXT)''',
                    "ops": '''CREATE TABLE ops
                                (urlName TEXT,
                                 objectPath TEXT, utf8Name TEXT,
                                 version TEXT, namespace TEXT,
                                 operation TEXT, type TEXT,
                                 size TEXT, retention TEXT,
                                 retentionString TEXT, retentionClass TEXT,
                                 ingestTimeString TEXT, ingestTime TEXT,
                                 accessTimeString TEXT, accessTime TEXT,
                                 changeTimeString TEXT, changeTimeMilliseconds TEXT,
                                 updateTimeString TEXT, updateTime TEXT,
                                 hashScheme TEXT, hash TEXT,
                                 acl TEXT, dpl TEXT,
                                 customMetadata TEXT, hold TEXT,
                                 indexed TEXT, replicated TEXT,
                                 shred TEXT, permissions TEXT,
                                 owner TEXT, uid TEXT,
                                 gid TEXT)''',
                    }
    
        # initialize the needed database tables
        for k in dbSchema.keys():
                self.ic.execute(dbSchema[k])
    
        # write admin record
        self.ic.execute('''INSERT INTO admin VALUES (?,?,?)''',
                   ('hcpmqt', "{0}".format(gVars.Version),
                    time.strftime("%a, %Y/%m/%d, %H:%M:%S",
                              time.localtime(time.time()))))
        
        self.dbConn.close()
        
    def writeItem(self, p):
        'Write a new record into table "obs"'
        return(self.dbConn.execute('''INSERT INTO ops
                                (urlName, objectPath, utf8Name,
                                 version, namespace, operation, type,
                                 size, retention, retentionString, retentionClass,
                                 ingestTimeString, ingestTime, accessTimeString,
                                 accessTime, changeTimeString, changeTimeMilliseconds,
                                 updateTimeString, updateTime, hashScheme, hash,
                                 acl, dpl, customMetadata, hold,
                                 indexed, replicated, shred, permissions,
                                 owner, uid, gid)
                                 VALUES (?,?,?,?,?,?,?,?,?,?,
                                 ?,?,?,?,?,?,?,?,?,?,
                                 ?,?,?,?,?,?,?,?,?,?,
                                 ?,?)''',
                                 (p['urlName'], p['objectPath'], p['utf8Name'],
                                  p['version'], p['namespace'], p['operation'],
                                  p['type'], p['size'], p['retention'],
                                  p['retentionString'], p['retentionClass'],
                                  p['ingestTimeString'],p['ingestTime'],
                                  p['accessTimeString'],p['accessTime'],
                                  p['changeTimeString'],p['changeTimeMilliseconds'],
                                  p['updateTimeString'],p['updateTime'],
                                  p['hashScheme'], p['hash'],p['acl'],
                                  p['dpl'],p['customMetadata'],p['hold'],
                                  p['index'],p['replicated'],p['shred'],
                                  p['permissions'],p['owner'],
                                  p['uid'],p['gid'])))
    
    def run(self):
        '''start the threads activity - waiting for elements to appear
        in the queue and write them into the database'''
        cnt = 0
        ## open database
        try:
            self.dbConn = sqlite3.connect(self.filename)
            self.ic = self.dbConn.cursor()
        except sqlite3.DatabaseError as e:
            raise(e)
        
        while True:
            record = self.rcvQueue.get()
            if record == '***finished***':
                self.rcvQueue.task_done()
                break
            try:
                self.writeItem(record)
            except sqlite3.IntegrityError as e:
                print(str(e), record, file=sys.stdout)
            self.rcvQueue.task_done()
            cnt += 1
            if not cnt%1000:
                self.dbConn.commit()
        
        self.dbConn.commit()
        self.dbConn.close()
