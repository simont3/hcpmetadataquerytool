# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>

import os
import time
import base64
import logging
from PyQt4 import QtCore, QtGui, QtNetwork
from hcpmqt.hcpmqtinit import gVars, iVars
from hcpmqt.hcpmqtquery import queryWorker as queryWorker
from hcpmqt.ui.ui_hcpmqt import Ui_MainWindow
from hcpmqt.ui.ui_hcpmqthelp import Ui_dialogHelp

class helpDialog(QtGui.QDialog):
    'Show the help dialog'
    def __init__(self, *args):
        QtGui.QDialog.__init__(self, *args)
        self.ui = Ui_dialogHelp()
        self.ui.setupUi(self)


class MyMainWindow(QtGui.QMainWindow):
    'Setup the main windows'
    def __init__(self, *args):
        QtGui.QMainWindow.__init__(self, *args)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.cancelvar = False
        self.ui.queryrunning = False
        self.ui.pause = False
        self.ui.logfile = None

        self.queryThread = queryWorker(parent=self)

        self.ui.pushButton_pause.setVisible(False)
        self.createConnects()
        self.readSettings()

     
    
    def writeSettings(self):
        'store this sessions paramaters for later use'
        settings = QtCore.QSettings("Thorsten Simons",
                                    "HCP Metadata Query Tool")
        settings.beginGroup('hcp access parameters')
        settings.setValue("target hcp", self.ui.lineEdit_targethcp.text())
        settings.setValue("namespaces", self.ui.lineEdit_namespaces.text())
        settings.setValue("directories", self.ui.lineEdit_directories.text())
        settings.setValue("user", self.ui.lineEdit_user.text())
        settings.setValue("page count", self.ui.spinBox_pageCount.value())
        settings.setValue("throttle", self.ui.spinBox_throttle.value())
        settings.endGroup()
        
        settings.beginGroup('output parameters')
        settings.setValue("csv", self.ui.radioButton_csv.isChecked())
        settings.setValue("sqlite3", self.ui.radioButton_sqlite3.isChecked())
        settings.setValue("output file", self.ui.lineEdit_outputfile.text())
        settings.setValue("verbose", self.ui.checkBox_verbose.isChecked())
        settings.setValue("dirstats", self.ui.checkBox_dirstats.isChecked())
        settings.endGroup()
        
        settings.beginGroup('transactions')
        settings.setValue("create", self.ui.checkBox_create.isChecked())
        settings.setValue("delete", self.ui.checkBox_delete.isChecked())
        settings.setValue("dispose", self.ui.checkBox_dispose.isChecked())
        settings.setValue("prune", self.ui.checkBox_prune.isChecked())
        settings.setValue("purge", self.ui.checkBox_purge.isChecked())
        settings.endGroup()
        
    def readSettings(self):
        'Load last sessions parameters'
        settings = QtCore.QSettings("Thorsten Simons",
                                    "HCP Metadata Query Tool")
        settings.beginGroup('hcp access parameters')
        self.ui.lineEdit_targethcp.setText(settings.value("target hcp", defaultValue='admin.hcp.domain.com'))
        self.ui.lineEdit_namespaces.setText(settings.value("namespaces"))
        self.ui.lineEdit_directories.setText(settings.value("directories"))
        self.ui.lineEdit_user.setText(settings.value("user"))
        self.ui.spinBox_pageCount.setValue(int(settings.value("page count", defaultValue="2500")))
        self.ui.spinBox_throttle.setValue(settings.value("throttle", defaultValue=0))
        settings.endGroup()
        
        settings.beginGroup('output parameters')
        self.ui.checkBox_verbose.setChecked(settings.value("verbose", defaultValue=True, type=bool))
        self.ui.checkBox_dirstats.setChecked(settings.value("dirstats", defaultValue=False, type=bool))
        self.ui.radioButton_csv.setChecked(settings.value("csv", defaultValue=True, type=bool))
        self.ui.radioButton_sqlite3.setChecked(settings.value("sqlite3", defaultValue=False, type=bool))
        self.ui.lineEdit_outputfile.setText(settings.value("output file"))
        settings.endGroup()
        
        settings.beginGroup('transactions')
        self.ui.checkBox_create.setChecked(settings.value("create", defaultValue=True, type=bool))
        self.ui.checkBox_delete.setChecked(settings.value("delete", defaultValue=True, type=bool))
        self.ui.checkBox_dispose.setChecked(settings.value("dispose", defaultValue=True, type=bool))
        self.ui.checkBox_prune.setChecked(settings.value("prune", defaultValue=True, type=bool))
        self.ui.checkBox_purge.setChecked(settings.value("purge", defaultValue=True, type=bool))
        self.ui.lineEdit_starttime.setText(settings.value("starttime", defaultValue="1970/01/01 00:00:00"))
        self.ui.lineEdit_endtime.setText(settings.value("endtime",
            defaultValue=time.strftime('%Y/%m/%d %H:%M:%S',time.localtime(time.time()))))
        settings.endGroup()
        
        
    def createConnects(self):
        'connect required signals with matching slots'
        self.ui.pushButton_runQuery.clicked.connect(self.runQuery)
        self.ui.pushButton_pause.clicked.connect(self.togglePause)
        self.ui.pushButton_quit.clicked.connect(self.quitCancel)
        self.ui.actionExit.triggered.connect(self.quitCancel)
        self.ui.actionSelect_Logfile.triggered.connect(self.chooseLogFile)
        self.ui.actionHelp.triggered.connect(self.showHelp)
        self.ui.actionAbout.triggered.connect(self.showAbout)
        self.ui.actionAbout_Qt.triggered.connect(self.showAboutQt)
        self.ui.actionAbout_GPL_V_3.triggered.connect(self.showGPL)
        self.ui.radioButton_csv.clicked.connect(self.switchFiletype)
        self.ui.radioButton_sqlite3.clicked.connect(self.switchFiletype)
        
        self.ui.lineEdit_starttime.editingFinished.connect(lambda: self.checkTimeEntry('Start time'))
        self.ui.lineEdit_endtime.editingFinished.connect(lambda: self.checkTimeEntry('End time'))
        self.ui.pushButton_resetTimeRange.clicked.connect(self.resetTimeRange)
        self.connect(self.queryThread, QtCore.SIGNAL("started()"), self.lockUi)
        self.connect(self.queryThread, QtCore.SIGNAL("finished()"), self.unlockUi)
        self.connect(self.queryThread, QtCore.SIGNAL("terminated()"), self.unlockUi)
        self.connect(self.queryThread, QtCore.SIGNAL("output(QString, QString, QString, QString)"), self.updateUi)
        
    
    @QtCore.pyqtSlot()
    def showAboutQt(self):
        QtGui.QMessageBox.aboutQt(self, gVars.Title)

    @QtCore.pyqtSlot()
    def showAbout(self):
        about = QtGui.QMessageBox(self)
        about.setTextFormat(2)
        about.setWindowTitle(gVars.Title)
        about.setIconPixmap(QtGui.QPixmap(":/images/data/resources/hcpmqt_128x128.png"))
        about.setText(gVars.Title + ' ' + gVars.Version)
        about.setInformativeText(gVars.About)
        about.setStandardButtons(QtGui.QMessageBox.Ok)
        about.setDefaultButton(QtGui.QMessageBox.Ok)
        about.exec_()
        del about

    @QtCore.pyqtSlot()
    def showGPL(self):
        helpWindow = helpDialog()
        helpWindow.ui.webView.setUrl(QtCore.QUrl("qrc:/about/src/COPYING.txt"))
        helpWindow.exec_()
        
    @QtCore.pyqtSlot()
    def showHelp(self):
        url = QtCore.QUrl("help/_build/html/index.html")
        helpWindow = helpDialog()
        helpWindow.ui.webView.setUrl(url)
        helpWindow.exec_()

    @QtCore.pyqtSlot()
    def switchFiletype(self):
        name = self.ui.lineEdit_outputfile.text()
        if name:
            name = os.path.splitext(name) 
            if self.ui.radioButton_sqlite3.isChecked():
                self.ui.lineEdit_outputfile.setText(name[0]+'.sqlite3')
            if self.ui.radioButton_csv.isChecked():
                self.ui.lineEdit_outputfile.setText(name[0]+'.csv')

    @QtCore.pyqtSlot()
    def checkTimeEntry(self, field):
        'check time entry field for validity'
        if field == 'Start time':
            fnc = self.ui.lineEdit_starttime
        else:
            fnc = self.ui.lineEdit_endtime

        try:
            i = int(time.mktime(time.strptime(fnc.text(), iVars.timeFormat)))
        except OverflowError:
            # Input "1970/01/01 00:00:00" may result in an OverflowError
            # in specific cases where localtime is ahead of GMt, so
            # reset to zero if this is the case.
            i = 0
        except ValueError:
            try:
                i = int(fnc.text())
            except ValueError:
                self.ui.statusBar.showMessage('{0}: {1}'.format(field,
                                              iVars.timeErrorMessage))
                return(-1)
            else:
                if i < 0: i = 0
        self.ui.statusBar.showMessage('')
        return(i)

    @QtCore.pyqtSlot()
    def resetTimeRange(self):
        self.ui.lineEdit_starttime.setText('1970/01/01 00:00:00')
        self.ui.lineEdit_endtime.setText(time.strftime('%Y/%m/%d %H:%M:%S',
                                         time.localtime(time.time())))

    @QtCore.pyqtSlot()
    def chooseLogFile(self):
        'Select a logfile'
        path = QtGui.QDesktopServices.storageLocation(QtGui.QDesktopServices.DesktopLocation)
        filename = QtGui.QFileDialog.getSaveFileName(parent=self,
                         caption='Save as...', directory=path,
                         filter='text (*.log)')
        if filename:
            try:
                frm = logging.Formatter("%(asctime)s %(name)s.%(funcName)s.%(lineno)d: %(message)s") #, "%m/%d %H:%M:%S")
                fhandler = logging.FileHandler(os.path.normpath(filename), "w")
                fhandler.setFormatter(frm)
                l = logging.getLogger()
                l.addHandler(fhandler)
                l.setLevel(logging.DEBUG)
                
                self.ui.showLogfile.setText('Logfile: {}'.format(os.path.normpath(filename)))
                self.ui.actionSelect_Logfile.setEnabled(False)
                self.ui.actionSelect_Logfile.setVisible(False)
            except:
                pass
                QtGui.QMessageBox.critical(self, 'Logging...', 'Logfile initialization failed!')
            

    @QtCore.pyqtSlot()
    def chooseFile(self):
        'Select an output file'
        if self.ui.radioButton_csv.isChecked():
            extFlt = 'Comma separated (*.csv)'
        else:
            extFlt = 'SQlite3 database (*.sqlite3)'
        if not self.ui.lineEdit_outputfile.text():
            path = QtGui.QDesktopServices.storageLocation(QtGui.QDesktopServices.DesktopLocation)
        else:
            path = os.path.dirname(self.ui.lineEdit_outputfile.text())
        filename = QtGui.QFileDialog.getSaveFileName(parent=self,
                         caption='Save as...', directory=path,
                         filter=extFlt)
        if filename:
            self.ui.lineEdit_outputfile.setText(os.path.normpath(filename))
            

    @QtCore.pyqtSlot()
    def quitCancel(self):
        if self.ui.pushButton_quit.text() == 'Cancel':
            answer = QtGui.QMessageBox.question(self, gVars.Title,
                                   "Are you sure you want to cancel the query?",
                                   QtGui.QMessageBox.Yes,
                                   QtGui.QMessageBox.No)
            if answer == QtGui.QMessageBox.Yes:
                self.queryThread.cancelQuery()
                self.ui.lineEdit_status.setText('Canceling - please stand by')
        else:
            self.writeSettings()
            QtCore.QCoreApplication.instance().quit()
        

    @QtCore.pyqtSlot()
    def togglePause(self):
        if self.ui.pause:
            self.ui.pushButton_pause.setText("Pause")
            self.ui.pushButton_quit.setEnabled(True)
            self.queryThread.togglePause(False)
            self.ui.pause = False
        else:
            self.ui.pushButton_pause.setText("Continue")
            self.ui.pushButton_quit.setEnabled(False)
            self.queryThread.togglePause(True)
            self.ui.pause = True

    @QtCore.pyqtSlot()
    def lockUi(self):
        'Lock the UI before processing a query'
        self.ui.pushButton_runQuery.setEnabled(False)
        self.ui.pushButton_quit.setText("Cancel")
        self.ui.pushButton_pause.setText("Pause")
        self.ui.pause = False
        self.ui.pushButton_pause.setVisible(True)
        self.ui.groupBox_output.setEnabled(False)
        self.ui.spinBox_throttle.setEnabled(True)
        self.ui.groupBox_hcp.setEnabled(False)
        self.ui.groupBox_query.setEnabled(False)
    
    @QtCore.pyqtSlot()
    def unlockUi(self):
        'Unlock the UI when done with processing a query'
        self.ui.pushButton_runQuery.setEnabled(True)
        self.ui.pushButton_quit.setText("Quit")
        self.ui.pushButton_pause.setVisible(False)
        self.ui.pushButton_pause.setText("Pause")
        self.ui.pause = False
        self.ui.groupBox_output.setEnabled(True)
        self.ui.groupBox_hcp.setEnabled(True)
        self.ui.groupBox_query.setEnabled(True)
    
    @QtCore.pyqtSlot(str, str, str, str)
    def updateUi(self, status, recordsFound, lastResult, ip):
        'Update the UI with data from the running query'
        if status:
            self.ui.lineEdit_status.setText(status)
        if recordsFound:
            self.ui.lineEdit_recordsFound.setText(recordsFound)
        if lastResult:
            self.ui.plainTextEdit_lastResult.setPlainText(lastResult)
        if ip:
            # Based on whatever reason I can't find, if the program is
            # frozen by cx_freeze, we don't get an ip-address as a string,
            # but as a strange hexadecimal string (\# 0caade1f'. So we
            # need to convert that back to a string in decimal ntation.
            if ip[1] == '#':
                hx = ip[-8:]
                # jetzt fehlt nur noch die uebersetzung von hex nach ip-adresse...
                try:
                    ip='{}.{}.{}.{}'.format(int(hx[:2],16),  int(hx[2:4],16),
                                            int(hx[4:6],16), int(hx[6:],16))
                except:
                    ip='--unidentified--'
            self.ui.label_ip.setText('[Using Node {}]'.format(ip))
    
    @QtCore.pyqtSlot()
    def runQuery(self):
        # clear status fields
        self.ui.lineEdit_status.setText('')
        self.ui.lineEdit_recordsFound.setText('')
        self.ui.plainTextEdit_lastResult.setPlainText('')
        self.ui.label_ip.setText('')
        
        # map the GUIs entries into a dict and send them to the 
        # query thread
        p = {'target hcp': '',
                      'namespaces': [],
                      'directories': [],
                      'user': '',
                      'password': '',
                      'count': 0,
                      'starttime': 0,
                      'endtime': 0,
                      'filetype': '',
                      'verbose': '',
                      'outputfile': '',
                      'dirstatfile': ''}

        # check the time fields for entries
        p['starttime'] = self.checkTimeEntry('Start time')
        if p['starttime'] == -1:
            self.ui.lineEdit_starttime.setFocus()
            return()
        p['endtime'] = self.checkTimeEntry('End time')
        if p['endtime'] == -1:
            self.ui.lineEdit_endtime.setFocus()
            return()
        
        p['target hcp'] = self.ui.lineEdit_targethcp.text()
        p['namespaces'] = self.ui.lineEdit_namespaces.text().split(',')
        p['directories'] = self.ui.lineEdit_directories.text().split(',')
        p['user'] = self.ui.lineEdit_user.text()
        p['password'] = self.ui.lineEdit_password.text()
        p['count'] = int(self.ui.spinBox_pageCount.value())
        if self.ui.radioButton_csv.isChecked():
            p['filetype'] = 'csv'
        else:
            p['filetype'] = 'sqlite3'
        if self.ui.checkBox_verbose.isChecked():
            p['verbose'] = 'true'
        else:
            p['verbose'] = 'false'
        p['outputfile'] = self.ui.lineEdit_outputfile.text()
        if self.ui.checkBox_dirstats.isChecked():
            p['dirstatfile'] = os.path.splitext(p['outputfile'])[0] + '.json'
        else:
            p['dirstatfile'] = ''
        p['transactions'] = []
        if self.ui.checkBox_create.isChecked():  p['transactions'].append('create')
        if self.ui.checkBox_delete.isChecked():  p['transactions'].append('delete')
        if self.ui.checkBox_dispose.isChecked(): p['transactions'].append('dispose')
        if self.ui.checkBox_prune.isChecked():   p['transactions'].append('prune')
        if self.ui.checkBox_purge.isChecked():   p['transactions'].append('purge')
        # send parameters to queryThread and start the thread
        self.queryThread.setParamsAndRun(p)
        
        
