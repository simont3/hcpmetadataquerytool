# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui\hcpmqt.ui'
#
# Created: Tue Jan  7 19:41:52 2014
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(710, 558)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/data/resources/HCPmqt.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.groupBox_hcp = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_hcp.setObjectName(_fromUtf8("groupBox_hcp"))
        self.gridLayout_2 = QtGui.QGridLayout(self.groupBox_hcp)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label_4 = QtGui.QLabel(self.groupBox_hcp)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout_2.addWidget(self.label_4, 4, 0, 1, 1)
        self.label_0 = QtGui.QLabel(self.groupBox_hcp)
        self.label_0.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_0.setObjectName(_fromUtf8("label_0"))
        self.gridLayout_2.addWidget(self.label_0, 0, 0, 1, 1)
        self.lineEdit_targethcp = QtGui.QLineEdit(self.groupBox_hcp)
        self.lineEdit_targethcp.setInputMethodHints(QtCore.Qt.ImhUrlCharactersOnly)
        self.lineEdit_targethcp.setMaxLength(2048)
        self.lineEdit_targethcp.setObjectName(_fromUtf8("lineEdit_targethcp"))
        self.gridLayout_2.addWidget(self.lineEdit_targethcp, 0, 1, 1, 1)
        self.lineEdit_directories = QtGui.QLineEdit(self.groupBox_hcp)
        self.lineEdit_directories.setMaxLength(2048)
        self.lineEdit_directories.setObjectName(_fromUtf8("lineEdit_directories"))
        self.gridLayout_2.addWidget(self.lineEdit_directories, 2, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.groupBox_hcp)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout_2.addWidget(self.label_3, 3, 0, 1, 1)
        self.label_1 = QtGui.QLabel(self.groupBox_hcp)
        self.label_1.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_1.setObjectName(_fromUtf8("label_1"))
        self.gridLayout_2.addWidget(self.label_1, 1, 0, 1, 1)
        self.lineEdit_namespaces = QtGui.QLineEdit(self.groupBox_hcp)
        self.lineEdit_namespaces.setMaxLength(2048)
        self.lineEdit_namespaces.setObjectName(_fromUtf8("lineEdit_namespaces"))
        self.gridLayout_2.addWidget(self.lineEdit_namespaces, 1, 1, 1, 1)
        self.label_2 = QtGui.QLabel(self.groupBox_hcp)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_2.addWidget(self.label_2, 2, 0, 1, 1)
        self.lineEdit_user = QtGui.QLineEdit(self.groupBox_hcp)
        self.lineEdit_user.setMaxLength(63)
        self.lineEdit_user.setObjectName(_fromUtf8("lineEdit_user"))
        self.gridLayout_2.addWidget(self.lineEdit_user, 3, 1, 1, 1)
        self.lineEdit_password = QtGui.QLineEdit(self.groupBox_hcp)
        self.lineEdit_password.setMaxLength(63)
        self.lineEdit_password.setEchoMode(QtGui.QLineEdit.Password)
        self.lineEdit_password.setObjectName(_fromUtf8("lineEdit_password"))
        self.gridLayout_2.addWidget(self.lineEdit_password, 4, 1, 1, 1)
        self.gridLayout.addWidget(self.groupBox_hcp, 0, 0, 1, 1)
        self.groupBox_status = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_status.setTitle(_fromUtf8(""))
        self.groupBox_status.setFlat(False)
        self.groupBox_status.setObjectName(_fromUtf8("groupBox_status"))
        self.gridLayout_5 = QtGui.QGridLayout(self.groupBox_status)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.label_status = QtGui.QLabel(self.groupBox_status)
        self.label_status.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_status.setObjectName(_fromUtf8("label_status"))
        self.gridLayout_5.addWidget(self.label_status, 0, 0, 1, 1)
        self.lineEdit_status = QtGui.QLineEdit(self.groupBox_status)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        self.lineEdit_status.setPalette(palette)
        self.lineEdit_status.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.lineEdit_status.setMouseTracking(False)
        self.lineEdit_status.setFocusPolicy(QtCore.Qt.NoFocus)
        self.lineEdit_status.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.lineEdit_status.setAcceptDrops(False)
        self.lineEdit_status.setText(_fromUtf8(""))
        self.lineEdit_status.setMaxLength(256)
        self.lineEdit_status.setFrame(False)
        self.lineEdit_status.setReadOnly(True)
        self.lineEdit_status.setPlaceholderText(_fromUtf8(""))
        self.lineEdit_status.setObjectName(_fromUtf8("lineEdit_status"))
        self.gridLayout_5.addWidget(self.lineEdit_status, 0, 1, 1, 1)
        self.lineEdit_recordsFound = QtGui.QLineEdit(self.groupBox_status)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        self.lineEdit_recordsFound.setPalette(palette)
        self.lineEdit_recordsFound.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.lineEdit_recordsFound.setMouseTracking(False)
        self.lineEdit_recordsFound.setFocusPolicy(QtCore.Qt.NoFocus)
        self.lineEdit_recordsFound.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.lineEdit_recordsFound.setAcceptDrops(False)
        self.lineEdit_recordsFound.setText(_fromUtf8(""))
        self.lineEdit_recordsFound.setFrame(False)
        self.lineEdit_recordsFound.setReadOnly(True)
        self.lineEdit_recordsFound.setPlaceholderText(_fromUtf8(""))
        self.lineEdit_recordsFound.setObjectName(_fromUtf8("lineEdit_recordsFound"))
        self.gridLayout_5.addWidget(self.lineEdit_recordsFound, 2, 1, 1, 1)
        self.label_lastResult = QtGui.QLabel(self.groupBox_status)
        self.label_lastResult.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTop|QtCore.Qt.AlignTrailing)
        self.label_lastResult.setObjectName(_fromUtf8("label_lastResult"))
        self.gridLayout_5.addWidget(self.label_lastResult, 3, 0, 1, 1)
        self.label_recordsFound = QtGui.QLabel(self.groupBox_status)
        self.label_recordsFound.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_recordsFound.setObjectName(_fromUtf8("label_recordsFound"))
        self.gridLayout_5.addWidget(self.label_recordsFound, 2, 0, 1, 1)
        self.plainTextEdit_lastResult = QtGui.QPlainTextEdit(self.groupBox_status)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        self.plainTextEdit_lastResult.setPalette(palette)
        self.plainTextEdit_lastResult.setFocusPolicy(QtCore.Qt.NoFocus)
        self.plainTextEdit_lastResult.setAcceptDrops(False)
        self.plainTextEdit_lastResult.setFrameShape(QtGui.QFrame.NoFrame)
        self.plainTextEdit_lastResult.setFrameShadow(QtGui.QFrame.Plain)
        self.plainTextEdit_lastResult.setLineWidth(0)
        self.plainTextEdit_lastResult.setTabStopWidth(4)
        self.plainTextEdit_lastResult.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.plainTextEdit_lastResult.setMaximumBlockCount(0)
        self.plainTextEdit_lastResult.setBackgroundVisible(True)
        self.plainTextEdit_lastResult.setCenterOnScroll(False)
        self.plainTextEdit_lastResult.setObjectName(_fromUtf8("plainTextEdit_lastResult"))
        self.gridLayout_5.addWidget(self.plainTextEdit_lastResult, 3, 1, 1, 1)
        self.gridLayout.addWidget(self.groupBox_status, 3, 0, 1, 2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_ip = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(7)
        font.setItalic(False)
        self.label_ip.setFont(font)
        self.label_ip.setText(_fromUtf8(""))
        self.label_ip.setTextFormat(QtCore.Qt.PlainText)
        self.label_ip.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.label_ip.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.label_ip.setObjectName(_fromUtf8("label_ip"))
        self.horizontalLayout.addWidget(self.label_ip)
        spacerItem = QtGui.QSpacerItem(400, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_pause = QtGui.QPushButton(self.centralwidget)
        self.pushButton_pause.setEnabled(True)
        self.pushButton_pause.setObjectName(_fromUtf8("pushButton_pause"))
        self.horizontalLayout.addWidget(self.pushButton_pause)
        self.pushButton_runQuery = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_runQuery.sizePolicy().hasHeightForWidth())
        self.pushButton_runQuery.setSizePolicy(sizePolicy)
        self.pushButton_runQuery.setObjectName(_fromUtf8("pushButton_runQuery"))
        self.horizontalLayout.addWidget(self.pushButton_runQuery)
        self.pushButton_quit = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_quit.sizePolicy().hasHeightForWidth())
        self.pushButton_quit.setSizePolicy(sizePolicy)
        self.pushButton_quit.setObjectName(_fromUtf8("pushButton_quit"))
        self.horizontalLayout.addWidget(self.pushButton_quit)
        self.gridLayout.addLayout(self.horizontalLayout, 4, 0, 1, 2)
        self.groupBox_output = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_output.setObjectName(_fromUtf8("groupBox_output"))
        self.gridLayout_4 = QtGui.QGridLayout(self.groupBox_output)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.line = QtGui.QFrame(self.groupBox_output)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout_4.addWidget(self.line, 0, 6, 1, 1)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.checkBox_verbose = QtGui.QCheckBox(self.groupBox_output)
        self.checkBox_verbose.setObjectName(_fromUtf8("checkBox_verbose"))
        self.verticalLayout_3.addWidget(self.checkBox_verbose)
        self.checkBox_dirstats = QtGui.QCheckBox(self.groupBox_output)
        self.checkBox_dirstats.setObjectName(_fromUtf8("checkBox_dirstats"))
        self.verticalLayout_3.addWidget(self.checkBox_dirstats)
        self.gridLayout_4.addLayout(self.verticalLayout_3, 0, 7, 1, 1)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_6 = QtGui.QLabel(self.groupBox_output)
        self.label_6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.horizontalLayout_3.addWidget(self.label_6)
        self.radioButton_csv = QtGui.QRadioButton(self.groupBox_output)
        self.radioButton_csv.setChecked(True)
        self.radioButton_csv.setObjectName(_fromUtf8("radioButton_csv"))
        self.horizontalLayout_3.addWidget(self.radioButton_csv)
        self.radioButton_sqlite3 = QtGui.QRadioButton(self.groupBox_output)
        self.radioButton_sqlite3.setEnabled(True)
        self.radioButton_sqlite3.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.radioButton_sqlite3.setObjectName(_fromUtf8("radioButton_sqlite3"))
        self.horizontalLayout_3.addWidget(self.radioButton_sqlite3)
        self.pushButton_chooseFile = QtGui.QPushButton(self.groupBox_output)
        self.pushButton_chooseFile.setObjectName(_fromUtf8("pushButton_chooseFile"))
        self.horizontalLayout_3.addWidget(self.pushButton_chooseFile)
        self.verticalLayout_4.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_7 = QtGui.QLabel(self.groupBox_output)
        self.label_7.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_2.addWidget(self.label_7)
        self.lineEdit_outputfile = QtGui.QLineEdit(self.groupBox_output)
        self.lineEdit_outputfile.setMaxLength(4096)
        self.lineEdit_outputfile.setObjectName(_fromUtf8("lineEdit_outputfile"))
        self.horizontalLayout_2.addWidget(self.lineEdit_outputfile)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.gridLayout_4.addLayout(self.verticalLayout_4, 0, 5, 1, 1)
        self.gridLayout.addWidget(self.groupBox_output, 2, 0, 1, 1)
        self.groupBox_query = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_query.setObjectName(_fromUtf8("groupBox_query"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox_query)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.groupBox_transactiontypes = QtGui.QGroupBox(self.groupBox_query)
        self.groupBox_transactiontypes.setObjectName(_fromUtf8("groupBox_transactiontypes"))
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox_transactiontypes)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.checkBox_create = QtGui.QCheckBox(self.groupBox_transactiontypes)
        self.checkBox_create.setChecked(True)
        self.checkBox_create.setObjectName(_fromUtf8("checkBox_create"))
        self.verticalLayout.addWidget(self.checkBox_create)
        self.checkBox_delete = QtGui.QCheckBox(self.groupBox_transactiontypes)
        self.checkBox_delete.setChecked(True)
        self.checkBox_delete.setObjectName(_fromUtf8("checkBox_delete"))
        self.verticalLayout.addWidget(self.checkBox_delete)
        self.checkBox_dispose = QtGui.QCheckBox(self.groupBox_transactiontypes)
        self.checkBox_dispose.setChecked(True)
        self.checkBox_dispose.setObjectName(_fromUtf8("checkBox_dispose"))
        self.verticalLayout.addWidget(self.checkBox_dispose)
        self.checkBox_prune = QtGui.QCheckBox(self.groupBox_transactiontypes)
        self.checkBox_prune.setChecked(True)
        self.checkBox_prune.setObjectName(_fromUtf8("checkBox_prune"))
        self.verticalLayout.addWidget(self.checkBox_prune)
        self.checkBox_purge = QtGui.QCheckBox(self.groupBox_transactiontypes)
        self.checkBox_purge.setChecked(True)
        self.checkBox_purge.setObjectName(_fromUtf8("checkBox_purge"))
        self.verticalLayout.addWidget(self.checkBox_purge)
        self.verticalLayout_2.addWidget(self.groupBox_transactiontypes)
        self.groupBox_timerange = QtGui.QGroupBox(self.groupBox_query)
        self.groupBox_timerange.setObjectName(_fromUtf8("groupBox_timerange"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox_timerange)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.lineEdit_starttime = QtGui.QLineEdit(self.groupBox_timerange)
        self.lineEdit_starttime.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.lineEdit_starttime.setText(_fromUtf8(""))
        self.lineEdit_starttime.setMaxLength(32)
        self.lineEdit_starttime.setObjectName(_fromUtf8("lineEdit_starttime"))
        self.gridLayout_3.addWidget(self.lineEdit_starttime, 0, 2, 1, 1)
        self.label = QtGui.QLabel(self.groupBox_timerange)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit_endtime = QtGui.QLineEdit(self.groupBox_timerange)
        self.lineEdit_endtime.setInputMethodHints(QtCore.Qt.ImhNone)
        self.lineEdit_endtime.setText(_fromUtf8(""))
        self.lineEdit_endtime.setMaxLength(32)
        self.lineEdit_endtime.setObjectName(_fromUtf8("lineEdit_endtime"))
        self.gridLayout_3.addWidget(self.lineEdit_endtime, 1, 2, 1, 1)
        self.label_5 = QtGui.QLabel(self.groupBox_timerange)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout_3.addWidget(self.label_5, 1, 0, 1, 1)
        self.pushButton_resetTimeRange = QtGui.QPushButton(self.groupBox_timerange)
        self.pushButton_resetTimeRange.setObjectName(_fromUtf8("pushButton_resetTimeRange"))
        self.gridLayout_3.addWidget(self.pushButton_resetTimeRange, 2, 2, 1, 1)
        self.verticalLayout_2.addWidget(self.groupBox_timerange)
        self.gridLayout.addWidget(self.groupBox_query, 0, 1, 3, 1)
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_6 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_12 = QtGui.QLabel(self.groupBox)
        self.label_12.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_12.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.horizontalLayout_6.addWidget(self.label_12)
        self.spinBox_pageCount = QtGui.QSpinBox(self.groupBox)
        self.spinBox_pageCount.setMinimum(100)
        self.spinBox_pageCount.setMaximum(25000)
        self.spinBox_pageCount.setSingleStep(100)
        self.spinBox_pageCount.setObjectName(_fromUtf8("spinBox_pageCount"))
        self.horizontalLayout_6.addWidget(self.spinBox_pageCount)
        self.label_9 = QtGui.QLabel(self.groupBox)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.horizontalLayout_6.addWidget(self.label_9)
        self.spinBox_throttle = QtGui.QSpinBox(self.groupBox)
        self.spinBox_throttle.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.spinBox_throttle.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.spinBox_throttle.setMaximum(3600)
        self.spinBox_throttle.setObjectName(_fromUtf8("spinBox_throttle"))
        self.horizontalLayout_6.addWidget(self.spinBox_throttle)
        self.gridLayout_6.addLayout(self.horizontalLayout_6, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 710, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        self.menuLogging = QtGui.QMenu(self.menubar)
        self.menuLogging.setObjectName(_fromUtf8("menuLogging"))
        MainWindow.setMenuBar(self.menubar)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        MainWindow.setStatusBar(self.statusBar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setMenuRole(QtGui.QAction.QuitRole)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setIcon(icon)
        self.actionAbout.setMenuRole(QtGui.QAction.AboutRole)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.actionAbout_Qt = QtGui.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/data/resources/Qt.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAbout_Qt.setIcon(icon1)
        self.actionAbout_Qt.setMenuRole(QtGui.QAction.AboutQtRole)
        self.actionAbout_Qt.setObjectName(_fromUtf8("actionAbout_Qt"))
        self.actionHelp = QtGui.QAction(MainWindow)
        self.actionHelp.setObjectName(_fromUtf8("actionHelp"))
        self.actionAbout_GPL_V_3 = QtGui.QAction(MainWindow)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/data/resources/gnu.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAbout_GPL_V_3.setIcon(icon2)
        self.actionAbout_GPL_V_3.setObjectName(_fromUtf8("actionAbout_GPL_V_3"))
        self.actionAbout_Python = QtGui.QAction(MainWindow)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/data/resources/Python.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAbout_Python.setIcon(icon3)
        self.actionAbout_Python.setObjectName(_fromUtf8("actionAbout_Python"))
        self.showLogfile = QtGui.QAction(MainWindow)
        self.showLogfile.setCheckable(False)
        self.showLogfile.setEnabled(False)
        self.showLogfile.setObjectName(_fromUtf8("showLogfile"))
        self.actionSelect_Logfile = QtGui.QAction(MainWindow)
        self.actionSelect_Logfile.setObjectName(_fromUtf8("actionSelect_Logfile"))
        self.menuFile.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionHelp)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionAbout_GPL_V_3)
        self.menuHelp.addAction(self.actionAbout_Qt)
        self.menuHelp.addAction(self.actionAbout)
        self.menuLogging.addAction(self.showLogfile)
        self.menuLogging.addAction(self.actionSelect_Logfile)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuLogging.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.label_4.setBuddy(self.lineEdit_password)
        self.label_0.setBuddy(self.lineEdit_targethcp)
        self.label_3.setBuddy(self.lineEdit_user)
        self.label_1.setBuddy(self.lineEdit_namespaces)
        self.label_2.setBuddy(self.lineEdit_directories)
        self.label_6.setBuddy(self.radioButton_csv)
        self.label_7.setBuddy(self.lineEdit_outputfile)
        self.label.setBuddy(self.lineEdit_starttime)
        self.label_5.setBuddy(self.lineEdit_endtime)

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.pushButton_chooseFile, QtCore.SIGNAL(_fromUtf8("clicked()")), MainWindow.chooseFile)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.lineEdit_targethcp, self.lineEdit_namespaces)
        MainWindow.setTabOrder(self.lineEdit_namespaces, self.lineEdit_directories)
        MainWindow.setTabOrder(self.lineEdit_directories, self.lineEdit_user)
        MainWindow.setTabOrder(self.lineEdit_user, self.lineEdit_password)
        MainWindow.setTabOrder(self.lineEdit_password, self.checkBox_verbose)
        MainWindow.setTabOrder(self.checkBox_verbose, self.checkBox_dirstats)
        MainWindow.setTabOrder(self.checkBox_dirstats, self.checkBox_create)
        MainWindow.setTabOrder(self.checkBox_create, self.checkBox_delete)
        MainWindow.setTabOrder(self.checkBox_delete, self.checkBox_dispose)
        MainWindow.setTabOrder(self.checkBox_dispose, self.checkBox_prune)
        MainWindow.setTabOrder(self.checkBox_prune, self.checkBox_purge)
        MainWindow.setTabOrder(self.checkBox_purge, self.lineEdit_starttime)
        MainWindow.setTabOrder(self.lineEdit_starttime, self.lineEdit_endtime)
        MainWindow.setTabOrder(self.lineEdit_endtime, self.pushButton_runQuery)
        MainWindow.setTabOrder(self.pushButton_runQuery, self.pushButton_quit)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "HCP Metadata Query Tool", None))
        self.groupBox_hcp.setTitle(_translate("MainWindow", "HCP access parameters:", None))
        self.label_4.setText(_translate("MainWindow", "&Password:", None))
        self.label_0.setText(_translate("MainWindow", "Target &HCP FQDN:", None))
        self.lineEdit_targethcp.setToolTip(_translate("MainWindow", "enter the full qualified domain name of HCP", None))
        self.lineEdit_targethcp.setWhatsThis(_translate("MainWindow", "oups...", None))
        self.lineEdit_targethcp.setPlaceholderText(_translate("MainWindow", "tenant.hcp.your-domain.net", None))
        self.lineEdit_directories.setToolTip(_translate("MainWindow", "filter for directories", None))
        self.lineEdit_directories.setPlaceholderText(_translate("MainWindow", "/dir1,/dir2/subdir,...", None))
        self.label_3.setText(_translate("MainWindow", "&User:", None))
        self.label_1.setText(_translate("MainWindow", "&Namespace(s):", None))
        self.lineEdit_namespaces.setToolTip(_translate("MainWindow", "filter for \'namespace.tenant\'", None))
        self.lineEdit_namespaces.setPlaceholderText(_translate("MainWindow", "namespace.tenant,namespace.tenant,...", None))
        self.label_2.setText(_translate("MainWindow", "&Directories:", None))
        self.lineEdit_user.setToolTip(_translate("MainWindow", "enter an user who has the Search-role", None))
        self.lineEdit_user.setPlaceholderText(_translate("MainWindow", "User with Search role enabled", None))
        self.label_status.setText(_translate("MainWindow", "Status:", None))
        self.label_lastResult.setText(_translate("MainWindow", "Last result:", None))
        self.label_recordsFound.setText(_translate("MainWindow", "Records found:", None))
        self.pushButton_pause.setText(_translate("MainWindow", "Pause", None))
        self.pushButton_runQuery.setText(_translate("MainWindow", "Run Query", None))
        self.pushButton_quit.setText(_translate("MainWindow", "Quit", None))
        self.groupBox_output.setTitle(_translate("MainWindow", "Output parameters:", None))
        self.checkBox_verbose.setToolTip(_translate("MainWindow", "write all available metadata to output file", None))
        self.checkBox_verbose.setText(_translate("MainWindow", "Verbose", None))
        self.checkBox_dirstats.setToolTip(_translate("MainWindow", "add a second file with a summary of the directory structure", None))
        self.checkBox_dirstats.setText(_translate("MainWindow", "Dirtree", None))
        self.label_6.setText(_translate("MainWindow", "Output type:", None))
        self.radioButton_csv.setToolTip(_translate("MainWindow", "write the output as a comma separated file", None))
        self.radioButton_csv.setText(_translate("MainWindow", "csv", None))
        self.radioButton_sqlite3.setToolTip(_translate("MainWindow", "write the output as a SQlite3 database file", None))
        self.radioButton_sqlite3.setText(_translate("MainWindow", "sqlite3", None))
        self.pushButton_chooseFile.setText(_translate("MainWindow", "Choose file...", None))
        self.label_7.setText(_translate("MainWindow", "&Output file:", None))
        self.lineEdit_outputfile.setPlaceholderText(_translate("MainWindow", "path/file", None))
        self.groupBox_query.setTitle(_translate("MainWindow", "Query parameters:", None))
        self.groupBox_transactiontypes.setTitle(_translate("MainWindow", "Transaction types:", None))
        self.checkBox_create.setToolTip(_translate("MainWindow", "files (and versions) that actually exist in HCP", None))
        self.checkBox_create.setText(_translate("MainWindow", "create", None))
        self.checkBox_delete.setToolTip(_translate("MainWindow", "files that have been deleted by an application", None))
        self.checkBox_delete.setText(_translate("MainWindow", "delete", None))
        self.checkBox_dispose.setToolTip(_translate("MainWindow", "files that have been disposed when retention period expired", None))
        self.checkBox_dispose.setText(_translate("MainWindow", "dispose", None))
        self.checkBox_prune.setToolTip(_translate("MainWindow", "versions of files that have been automactically deleted after its time has passed", None))
        self.checkBox_prune.setText(_translate("MainWindow", "prune", None))
        self.checkBox_purge.setToolTip(_translate("MainWindow", "versions of files that have been deleted including all versions", None))
        self.checkBox_purge.setText(_translate("MainWindow", "purge", None))
        self.groupBox_timerange.setTitle(_translate("MainWindow", "Time range:", None))
        self.lineEdit_starttime.setToolTip(_translate("MainWindow", "Date/Time or seconds since 1970/01/01 00:00:00, the UNIX epoch", None))
        self.lineEdit_starttime.setPlaceholderText(_translate("MainWindow", "YYYY/MM/DD HH:MM:SS or seconds since 1970/01/01 00:00:00", None))
        self.label.setText(_translate("MainWindow", "&Start time:", None))
        self.lineEdit_endtime.setToolTip(_translate("MainWindow", "Date/Time or seconds since 1970/01/01 00:00:00, the UNIX epoch", None))
        self.lineEdit_endtime.setPlaceholderText(_translate("MainWindow", "YYYY/MM/DD HH:MM:SS or seconds since 1970/01/01 00:00:00", None))
        self.label_5.setText(_translate("MainWindow", "&End time:", None))
        self.pushButton_resetTimeRange.setText(_translate("MainWindow", "Reset to 1970/01/01 00:00:00 until NOW", None))
        self.groupBox.setTitle(_translate("MainWindow", "HCP load parameters:", None))
        self.label_12.setText(_translate("MainWindow", "Records / page:", None))
        self.label_9.setText(_translate("MainWindow", "Throttle (sec/page):", None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.menuHelp.setTitle(_translate("MainWindow", "Help", None))
        self.menuLogging.setTitle(_translate("MainWindow", "Logging", None))
        self.actionExit.setText(_translate("MainWindow", "Exit", None))
        self.actionAbout.setText(_translate("MainWindow", "About HCP Metadata Query Tool", None))
        self.actionAbout_Qt.setText(_translate("MainWindow", "About Qt", None))
        self.actionHelp.setText(_translate("MainWindow", "Help", None))
        self.actionAbout_GPL_V_3.setText(_translate("MainWindow", "GNU General Public License", None))
        self.actionAbout_Python.setText(_translate("MainWindow", "About Python", None))
        self.showLogfile.setText(_translate("MainWindow", "Logfile: None", None))
        self.actionSelect_Logfile.setText(_translate("MainWindow", "Select Logfile", None))

from . import hcpmqt_rc
