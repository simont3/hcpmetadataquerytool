# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui\hcpmqthelp.ui'
#
# Created: Tue Jan  7 19:41:52 2014
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_dialogHelp(object):
    def setupUi(self, dialogHelp):
        dialogHelp.setObjectName(_fromUtf8("dialogHelp"))
        dialogHelp.setWindowModality(QtCore.Qt.WindowModal)
        dialogHelp.resize(960, 700)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/data/resources/HCPmqt.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        dialogHelp.setWindowIcon(icon)
        dialogHelp.setSizeGripEnabled(True)
        self.verticalLayout = QtGui.QVBoxLayout(dialogHelp)
        self.verticalLayout.setContentsMargins(-1, -1, 12, -1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.webView = QtWebKit.QWebView(dialogHelp)
        self.webView.setObjectName(_fromUtf8("webView"))
        self.verticalLayout.addWidget(self.webView)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.pushButton_back = QtGui.QPushButton(dialogHelp)
        self.pushButton_back.setObjectName(_fromUtf8("pushButton_back"))
        self.horizontalLayout_3.addWidget(self.pushButton_back)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.pushButton_Cancel = QtGui.QPushButton(dialogHelp)
        self.pushButton_Cancel.setObjectName(_fromUtf8("pushButton_Cancel"))
        self.horizontalLayout_3.addWidget(self.pushButton_Cancel)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(dialogHelp)
        QtCore.QObject.connect(self.pushButton_Cancel, QtCore.SIGNAL(_fromUtf8("clicked()")), dialogHelp.close)
        QtCore.QObject.connect(self.pushButton_back, QtCore.SIGNAL(_fromUtf8("clicked()")), self.webView.back)
        QtCore.QMetaObject.connectSlotsByName(dialogHelp)

    def retranslateUi(self, dialogHelp):
        dialogHelp.setWindowTitle(_translate("dialogHelp", "HCP Metadata Query Tool", None))
        self.pushButton_back.setText(_translate("dialogHelp", "Back", None))
        self.pushButton_Cancel.setToolTip(_translate("dialogHelp", "press to dismiss the help window", None))
        self.pushButton_Cancel.setText(_translate("dialogHelp", "Quit", None))

from PyQt4 import QtWebKit
from . import hcpmqt_rc
