# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>

import os
import sys

# initialized variables
#
class gVars():
    '''
    gvars holds constants and variables that need to be present within the
    whole project.
    '''

    # version control
    _version        = "1.0.11"
    _build          = "2014-08-21/Sm"
    _minPython      = "3.3.2"
    _description    = "HCP Metadata Query Tool"
    _docuDir        = "Documentation"

    # constants
    Version         = "v.{0} ({1})".format(_version, _build)
    Author          = "Contact: sw@snomis.de"
    URL             = 'https://sourceforge.net/projects/hcpmetadataquer/'
    docuDir         = _docuDir
    Title           = _description
    About           = \
"""Query Hitachi Content Platform (HCP) for transactions on objects
(create, delete, dispose, prune, purge).

Copyright 2012-2014 Thorsten Simons <sw@snomis.de>
This program is licensed under the GNU GPL version 3.

Source code is available at www.sourceforge.net/projects/hcpmetadataquer

Powered by:
Python {1}\t\t(http://www.python.org)
PyQt GPL v4.10.3\t(http://www.riverbankcomputing.co.uk/software/pyqt)
dnspython 1.11.1\t(http://www.dnspython.org)
Sphinx 1.2b1\t\t(http://sphinx-doc.org)
cx_freeze 4.3.2\t\t(http://cx-freeze.sourceforge.net/)
""".format(_description, sys.version[:5])


class iVars():
    fieldnames = ['urlName', 'objectPath', 'utf8Name', 'version', 'namespace', 
                  'operation', 'type', 'size', 
                  'retention', 'retentionString', 'retentionClass',
                  'ingestTimeString', 'ingestTime',
                  'accessTimeString', 'accessTime',
                  'changeTimeString', 'changeTimeMilliseconds',
                  'updateTimeString', 'updateTime',
                  'hashScheme', 'hash',  
                  'acl', 'dpl', 'customMetadata','hold', 'index',
                  'replicated', 'shred',
                  'permissions', 'owner', 'uid', 'gid',
                 ]
    defaults = {'target hcp': 'admin.hcp.company.local',
                'namespaces': '',
                'directories': '',
                'user': 'mqe',
                'records': '1000',
                'format': 'csv',
                'dirtree': 0,
                'verbose': 1,
                'output file': '{0}\\hcpmqt.csv'.format(os.path.expanduser('~\\HCPmqt')),
                'tvar': [1,1,1,1,1],
                'tvarstr': ['create', 'delete', 'dispose','prune','purge'],
                'start time': '',
                'end time': '',
                }
    
    timeFormat = '%Y/%m/%d %H:%M:%S'
    timeErrorMessage = 'invalid entry - use YYYY/MM/DD HH:MM:SS (or seconds since 1970.01.01 00:00:00)'
