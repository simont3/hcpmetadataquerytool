# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>

import os
import http.client
import base64
import hashlib
import time
import csv
import json
import socket
import queue
import dns.resolver
import ipaddress
import logging
from urllib.parse import urlsplit
from tempfile import TemporaryFile
from xml.sax import handler, parseString
from xml.etree.ElementTree import TreeBuilder, ElementTree
from PyQt4 import QtCore

from hcpmqt.hcpmqtinit import iVars
from hcpmqt.hcpmqtdb import dbHandler

#=======================================================================
# mkRequest - build the xml-request for a query
#=======================================================================
def mkRequest(count=None, namespaces=[], directories=[], transactions=[],
              starttime=0, endtime=0,
              verbosity='true', urlName=None, cTms=None, version=None):
    '''
    Build a XML-structure containg the query; return an open filehandle,
    set to position 0, to an unnamed tempfile holding the XML.  
    '''
    logger = logging.getLogger(__name__)
    
    # set default behaviour
    if not count:
        count = 1000
        
    tree = TreeBuilder()
    tree.start("queryRequest", {})
    tree.start("operation", {})
    tree.start("count", {})
    tree.data("{0}".format(count))
    tree.end("count")
    tree.start("systemMetadata", {})
    if namespaces != ['']:
        tree.start("namespaces", {})
        for ns in namespaces:
            tree.start("namespace", {})
            tree.data("{0}".format(ns))
            tree.end("namespace")
        tree.end("namespaces")
    if directories != ['']:
        tree.start("directories", {})
        for d in directories:
            tree.start("directory", {})
            tree.data("{0}".format(d))
            tree.end("directory")
        tree.end("directories")
    if transactions:
        tree.start("transactions", {})
        for ta in transactions:
            tree.start("transaction", {})
            tree.data("{0}".format(ta))
            tree.end("transaction")
        tree.end("transactions")
#    if starttime or endtime:
    tree.start('changeTime', {})
    tree.start('start',{})
    tree.data(str(starttime*1000))
    tree.end('start')
    tree.start('end',{})
    tree.data(str(endtime*1000))
    tree.end('end')
    tree.end('changeTime')
    tree.end("systemMetadata")
    # if we have a paged query, this is for page 2 and following...
    if urlName:
        tree.start("lastResult", {})
        tree.start("urlName", {})
        tree.data(str(urlName))
        tree.end("urlName")
        tree.start("changeTimeMilliseconds", {})
        tree.data(str(cTms))
        tree.end("changeTimeMilliseconds")
        tree.start("version", {})
        tree.data(str(version))
        tree.end("version")
        tree.end("lastResult")
    tree.start("verbose", {})
    tree.data(verbosity)
    tree.end("verbose")
    tree.end("operation")
    tree.end("queryRequest")
    elem = tree.close()
    e = ElementTree(elem)
    
    # open a temporary file to hold the XML query
    thdl = TemporaryFile()
    e.write(thdl, encoding='UTF-8', xml_declaration=True)
#    with open('c:\\users\\sm\\desktop\\request.txt', 'w') as rHdl:
#        e.write(rHdl, encoding='UTF-8', xml_declaration=True)
    thdl.seek(0,0)

    return(thdl)


## from a given no. of seconds, calculate a time string
#
def calcTime(t):
    msec = int("{0:.2f}".format(t%1)[2:])
    min1  = int(t//60)
    sec  = int(t%60)
    hour = int(min1//60)
    min2  = int(min1%60)
    return("{0:02}:{1:02}:{2:02}.{3}".format(hour, min2, sec, msec))


# xml parser class
class saxHandler(handler.ContentHandler):
    
    def __init__(self):
        handler.ContentHandler.__init__(self)
        self.objectTags = iVars.fieldnames 
        self.query = {}
        self.object = {}
        self.status = {}
        self.csvHdl = None
        self.sqlite3Hdl = None

    def setcsvHdl(self, csvHdl):
        self.csvHdl = csvHdl
    
    def setsqlite3Hdl(self, sndQueue=None):
        self.sndQueue = sndQueue

    def setTreeStructure(self, treeStructure=None):
        self.treeSt = treeStructure

    def startElement(self, name, attrs):
        if name == 'query':
            self.query['start'] = attrs.get('start')
            self.query['end'] = attrs.get('end')
        if name == 'object':
            # make sure, that only available tags go into the record
            # (skip 'None's)
            
            for i in self.objectTags:
                if attrs.get(i) != None:
                    self.object[i] = attrs.get(i)
                else:
                    self.object[i] = ''
                
            # remember the last object in this request for later use
            # (in paged queries)
            self.status['urlName'] = self.object['urlName']
            self.status['changeTimeMilliseconds'] = self.object['changeTimeMilliseconds']
            self.status['version'] = self.object['version']
            
            # write the complete record
            if self.csvHdl:
                self.csvHdl.writerow(self.object)
            else: # --> sqlite3
                self.sndQueue.put(self.object)
                #self.sqliteHdl.writeItem(self.object)
            if self.treeSt != None:
                if self.object['operation'] == 'CREATED':
                    self.addStructure(self.object['urlName'])
            self.object = {}
        if name == 'status':
            self.status['results'] = attrs.get('results')
            self.status['message'] = attrs.get('message')
            self.status['code'] = attrs.get('code')

    #===================================================================
    # addStructure - adds an entry into the dirtree structure
    #===================================================================
    def addStructure(self, urlname):
        '''
        Take an URL, split it up into its relevant parts and fill those
        parts into a dictionary, building up a tree of the directories
        involved. Count the no. of files and subdirs per directory.
        '''
        # split all components to get a sorted list of all path components:
        # ['hcp.x.y', 'tenant', 'namespace', 'dir', 'dir', ..., 'file']
        spliturl = urlsplit(urlname)
        tmpsplit = spliturl[1].split('.', 2)
        pathcomponents = [tmpsplit[2], tmpsplit[1], tmpsplit[0]]
        for i in spliturl[2].split('/')[1:]:
            pathcomponents.append(i)

        # work recursively through the list of path components and build
        # up the structure
        cnt = 0
        anz = len(pathcomponents)
        s = self.treeSt            # initially, point s to the structure
        while cnt < anz:             # --> haven't reached the last PART
            if cnt < anz-1:                  # --> PART not in s --> add
                if not pathcomponents[cnt] in s:
                    if cnt >= 3:
                        s[pathcomponents[cnt]] = {'_#files#_':0,'_#subdirs#_':0}
                    else:
                        s[pathcomponents[cnt]] = {}
                    if cnt > 3:
                        s['_#subdirs#_'] += 1
            else:            # or: this is the last PART (the file name)
                s['_#files#_'] += 1
            if cnt < anz-1:
                s = s[pathcomponents[cnt]]
            cnt += 1


#=======================================================================
# query - query thread
#=======================================================================
class queryWorker(QtCore.QThread):

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        self.p = {'target hcp': '',
                  'namespaces': [],
                  'directories': [],
                  'user': '',
                  'password': '',
                  'count': 0,
                  'starttime': 0,
                  'endtime': 0,
                  'filetype': '',
                  'verbose': '',
                  'outputfile': '',
                  'dirstatfile': ''}
        self.cancel = False
        self.pause = False
        self.ip = None           # store the ip address of node actually in use
        self.parent = parent
        
    def __del__(self):
        'make sure the thread cancels cleanly'
        self.cancel = True
        self.wait()

    def togglePause(self, mode):
        'toggle pause True=pause, False=run'
        self.pause = mode        
    
    def cancelQuery(self):
        'request cancelation of a running query'
        self.cancel = True
    
    def setParamsAndRun(self, params):
        'set the parameters needed to run the query'
        self.p = params
        self.cancel = False
        self.pause = False
        # start db thread if desired
        if self.p['filetype'] == 'sqlite3':
            ## start dbWriter thread and setup its Queue
            #
            self.p['queue'] = queue.Queue()
            self.dbWriterThread = dbHandler(self.p['outputfile'], self.p['queue'])
            self.dbWriterThread.name = "dbWriter"
            self.dbWriterThread.daemon = True
        self.start()

    def emitSignal(self, status='', recordsFound='', lastResult='', ip=''):
        'emit a signal to main thread'
        self.emit(QtCore.SIGNAL("output(QString, QString, QString, QString)"),
                                status, recordsFound, lastResult, str(ip))
#                                status, recordsFound, lastResult, base64.b64encode(str(ip).encode()).decode())
        
    
    def run(self):
        '''Query HCP for pages of data repeatedly until all pages have
        been received.'''
        logger = logging.getLogger(__name__)
        
        startTime = time.time()
        fail = ''
        fndRecs = 0
        logger.debug('start query')
            
        # make sure we have a FQDN and not an ip-address
        try:
            x = ipaddress.ip_address(self.p['target hcp'])
        except ValueError:
            pass
        else:
            logger.error("{} isn't a full qualified domain name!".format(self.p['target hcp']))
            self.emitSignal(status="{} isn't a full qualified domain name!".format(self.p['target hcp']))
            return
        
        # resolve HCPs IP addresses (we should talk always to the
        # same node to keep db load as low as possible
        try:
            self.ip = str(dns.resolver.query(self.p['target hcp'])[0])
        except:
            logger.error('DNS query for {} failed - NXDOMAIN'.format(self.p['target hcp']))
            self.emitSignal(status='DNS query for {} failed - NXDOMAIN'.format(self.p['target hcp']))
            return
        else:
            if self.ip[1] == '#':
                hx = self.ip[-8:]
                # jetzt fehlt nur noch die uebersetzung von hex nach ip-adresse...
                try:
                    self.ip='{}.{}.{}.{}'.format(int(hx[:2],16),
                                                 int(hx[2:4],16), int(hx[4:6],16), int(hx[6:],16))
                except:
                    ip='--unidentified--'
            logger.info('DNS query succeeded - ip={}'.format(self.ip))
            self.emitSignal(status='DNS query succeeded', ip=self.ip)

        # make dir if needed!
        try:
            os.makedirs(os.path.dirname(self.p['outputfile']), exist_ok=True)
            if self.p['filetype'] == 'csv':
                fhdl = open(self.p['outputfile'], 'w', newline='')
            elif self.p['filetype'] == 'sqlite3':
                self.dbWriterThread.start()
            # init the building of the dirtree structure
            if self.p['dirstatfile']:
                treeStructure = {}
            else:
                treeStructure = None
        except (OSError, IOError, WindowsError) as e:
            logger.error('Can\'t open outputfile: {0}'.\
                                format(str(e).replace('\\\\', '\\')))
            self.emitSignal(status='Can\'t open outputfile: {0}'.\
                                format(str(e).replace('\\\\', '\\')))
        else:
            if self.p['filetype'] == 'csv':
                csvHdl = csv.DictWriter(fhdl, iVars.fieldnames, restval='',
                                        extrasaction='ignore', dialect='excel',
                                        quoting=csv.QUOTE_MINIMAL)
                csvHdl.writeheader()
            
            # pre-set the parameters for sub-sequent requests
            status = {'urlName': None,
                      'changeTimeMilliseconds': None,
                      'version': None,
                      'code': None}
            hdr={'cookie': "hcp-ns-auth=" + \
                        base64.b64encode(self.p['user'].encode()).decode() + \
                        ":" + hashlib.md5(self.p['password'].encode()).hexdigest(),
                 'Accept': 'application/xml',
                 'Content-Type': 'application/xml',
                 'Host': self.p['target hcp']}
            
            first = True
            while status['code'] != 'COMPLETE':
                if self.cancel:
                    logger.info('query loop exited...')
                    break
                
                # if the Pause button is pressed, hold on here...
                while self.pause:
                    self.emitSignal(status='paused...')
                    time.sleep(1)
                else:
                    if first:
                        first = False
                    else:
                        x = self.parent.ui.spinBox_throttle.value()
                        if x:
                            if x > 2:
                                self.emitSignal(status='throttling for {} seconds...'.format(x))
                            time.sleep(x)
                    
                # flag used to close the http session if loop needs to be broken
                loopBroken = False
                
                # setup connection
                con = http.client.HTTPSConnection(self.ip)
#                con = http.client.HTTPSConnection(self.p['target hcp'])
                logger.info('Trying to connect to {}'.format(self.p['target hcp']))
                self.emitSignal(status='Trying to connect to {}'.format(self.p['target hcp']))
                
                # open connection
                try:
                    con.connect()
                    logger.info('Connection opened - requesting data')
                    self.emitSignal(status='Connection opened - requesting data')
                except (socket.error, socket.gaierror) as e:
                    fail = '{0}'.format(str(e))
                    loopBroken = True
                    logger.error('Connection failed - {}'.format(str(e)))
                    break

#                req = mkRequest(count=self.p['count'], urlName=status['urlName'],
                req = mkRequest(count=self.parent.ui.spinBox_pageCount.value(),
                                urlName=status['urlName'],
                                cTms=status['changeTimeMilliseconds'],
                                version=status['version'],
                                namespaces=self.p['namespaces'],
                                directories=self.p['directories'],
                                transactions=self.p['transactions'],
                                verbosity=self.p['verbose'],
                                starttime=self.p['starttime'],
                                endtime=self.p['endtime'])
                reqt = {"count": self.parent.ui.spinBox_pageCount.value(),
                        "urlName": status['urlName'],
                        "cTms": status['changeTimeMilliseconds'],
                        "version": status['version'],
                        "namespaces": self.p['namespaces'],
                        "directories": self.p['directories'],
                        "transactions": self.p['transactions'],
                        "verbosity": self.p['verbose'],
                        "starttime": self.p['starttime'],
                        "endtime": self.p['endtime']}
                logger.info('request: {}, {}, {}'.format('POST','/query',reqt))
                con.request('POST', '/query', body=req, headers=hdr)
                result = con.getresponse()
                if result.status == 200:
                    logger.info('result: {0} {1}, receiving data'.format(result.status, result.reason))                    
                    self.emitSignal(status="{0} {1}, receiving data".format(result.status, result.reason))
                else:
                    fail = '{0} {1} - Query failed'.format(result.status, result.reason)
                    logger.error('result: {}'.format(fail))
                    loopBroken = True
                    break
                handler = saxHandler()
                if self.p['filetype'] == 'csv':
                    handler.setcsvHdl(csvHdl)
                elif self.p['filetype'] == 'sqlite3':
                    handler.setsqlite3Hdl(self.p['queue'])
                handler.setTreeStructure(treeStructure)
                parseString(result.read(), handler)
                
                lastRecs = int(handler.status['results'])
                fndRecs = fndRecs + lastRecs
                self.emitSignal(recordsFound='{0:,}'.format(fndRecs))
                status = handler.status
                del(handler)
                s = ''
                for i in status:
                    s += "{0}: {1}\n".format(i, status[i])
                if status['code'] != 'COMPLETE':
                    self.emitSignal(status="requesting next page",
                                    lastResult=str(s))
                elif status['code'] == 'COMPLETE':
                    self.emitSignal(status="requesting next page",
                                            lastResult=str(s))
                logger.info('got {} (total {}) recs, last = {}'.format(lastRecs,fndRecs,str(s).replace('\n',' | ')))
                con.close()
            
            # if the loop has been broken, close the http session
            if loopBroken:
                con.close()
            
            if self.p['filetype'] == 'csv':
                    fhdl.close()
            
            # if required, write the tree file as a json file from the
            # dirtree structure
            if self.p['dirstatfile']:
                self.emitSignal(status='Writing tree file...')
                try:
                    with open(self.p['dirstatfile'], 'w') as thdl:
                        print(json.dumps(treeStructure, sort_keys=True,
                                         indent=4), file=thdl)
                except IOError as e:
                    self.emitSignal(status="Can't open tree file: {0}".\
                                                      format(str(e).replace('\\\\', '\\')))
                    del treeStructure
                
            if self.p['filetype'] == 'sqlite3':
                # wait for dbWriter to get finished
                self.p['queue'].put("***finished***")
                while self.p['queue'].qsize() > 1:
                    self.emitSignal(status="Received all data - {0} records left to be written to the database".\
                                    format(self.p['queue'].qsize()-1))
                    time.sleep(1)
                self.p['queue'].join()
                self.dbWriterThread.join()            
            if fail:
                self.emitSignal(status=fail)
                fail = ''
                self.cancel = False
            else:
                if self.cancel:
                    self.emitSignal(status='Query canceled (duration: {0})'.\
                                           format(calcTime(time.time() - startTime)))
                    logger.info('Query canceled (duration: {0})'.\
                                           format(calcTime(time.time() - startTime)))
                    self.cancel = False
                else:
                    logger.info('Query finished (duration: {0})'.\
                                           format(calcTime(time.time() - startTime)))
                    self.emitSignal(status='Query finished (duration: {0})'.\
                                           format(calcTime(time.time() - startTime)))

