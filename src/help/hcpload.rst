*******************
HCP load Parameters
*******************

.. WARNING::
	Using *HCPmqt* can put severe load on *HCP*, especially if there is a huge number of
	objects stored. You might want to monitor *HCP* performance during a query and tune the
	load parameters accordingly.
	
.. image:: images/hcpload.png
	:align: center

These values are intended to tune the load generated within *HCP* when running *HCPmqt*.

Use the *Records / page* field to specify the number of records that gets fetched from HCP
with a single call. Larger number speed things up a bit, but need more local memory - where
smaller number slow down things a bit, but need less memory. 5,000 to 10,000 is a value known
as good.

The *Throttle (sec/page)* field asks the tool to pause for the defined number of seconds
between subsequent page requests

.. TIP::
	Both values may be changed while a query is running. Please note that changes won't take
	place until the page in work has been processed.
	
