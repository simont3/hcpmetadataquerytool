******
Output
******

.. image:: images/hcpoutput.png
	:align: center

Two different output types are available:

* csv - comma separated values (used to import data into spreadsheet software
* sqlite3 - a single-file database [#f1]_

Normally, the output will hold selected information only: ``urlName, version, operation``
and ``changeTimeMilliseconds``.

If **Verbose** is checked, all information will be provided:
``urlName, objectPath, utf8Name, version, namespace, operation, type, size, retention,
retentionString, retentionClass, ingestTimeString, ingestTime, accessTimeString,
accessTime, changeTimeString, changeTimeMilliseconds, updateTimeString, updateTime,
hashScheme, hash, acl, dpl, customMetadata, hold, index, replicated, shred, permissions, owner, uid, gid``

.. TIP::
	If you need statistical data for a Namespace (or a HCP system at all), check **Dirtree**.
	This will write an additional file holding a JSON-structure containing the complete
	directory tree, including the number of files and subfolders per folder.
	



.. rubric:: Footnotes

.. [#f1] SQlite3 databases can be used by most programming languages. You can also discover them by using the *SQlite Shell* available from `sqlite.org`_ if you like to use the commandline; if you prefer a GUI, try the *SQLite Manager* Add-on for the Firefox Webbrowser.


.. _`sqlite.org`: http://sqlite.org/download.html
