*********************
HCP query Parameters
*********************

	.. image:: images/hcpquery.png
		:align: center

Select the type of operational records you want to get.

+-------------+------------------------------------------------------------------+
| Transaction | Description                                                      |
| type        |                                                                  |
+=============+==================================================================+
| create      | existing (!) objects                                             |
+-------------+------------------------------------------------------------------+
| delete      | objects that have been deleted                                   |
+-------------+------------------------------------------------------------------+
| dispose     | objects that have been automatically deleted by HCP after the    |
|             | objects retention had expired [#f1]_                             |
+-------------+------------------------------------------------------------------+
| prune       | object's versions that have been automatically deleted after     |
|             | their lifetime has passed [#f2]_                                 |
+-------------+------------------------------------------------------------------+
| purge       | object's versions that have been deleted when the *head*-object  |
|             | (the newest version) was deleted                                 |
+-------------+------------------------------------------------------------------+
	
	
	
.. rubric:: Footnotes

.. [#f1] Disposition will take place if the *Disposition Service* is enabled in the System Console and for the Namespace, too.

.. [#f2] Namespaces that are enabled for *Versioning* define a periode of time during that versions of objects are kept. After a version is leaving this periode of time, it will be pruned (deleted) automatically.