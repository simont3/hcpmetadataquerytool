*************
Prerequisites
*************

To be able to use *HCPmqt*, there are several prerequisites:

*	Using an system-level user account, log into the *System Console*:

	*	Navigate to *Security* / *Permissions* and make sure that the **Search** permission is checked
		within the *Systemwide Permission Mask*.
	
	*	A System-level user account can be used with *HCPmqt* for those Tenants that have delegated
		administrative rights to *System-level users*. In this case, the user account
		is required to have the *Search* role: navigate to *Security* / *Users* (or *Groups*), select
		the desired user in the list to open its panel. Make sure that **Search** is checked in the
		*Role* panel.

*	Using an Tenant user account, log into the *Tenant Management Console* of the Tenant to be queried:
	
	*	Check if the **Search** permission is set in the *Permissions* panel on the *Overview* page.
	
	*	Check that the **Search** permission is set for each Namespace to be queried.
			
	*	Make sure the account to be used with *HCPmqt* has the *Search* permission:
		Navigate to *Security* / *Users* (or *Groups*) and select the user from the list. In the list
		of Namespaces at the bottom, select the Namespaces of interest and make sure that **Search**
		is checked.
		
*	To use a data access user (a Tenant Account without administrative rights):

	*	use an administrative Tenant user to make sure that the respective user has the *Search* permission.

.. Tip::
	Tenants do not need to have the *Search* feature enabled to be queried by *HCPmqt*, nor is there
	a need to index the content!