******
Status
******

.. image:: images/hcpstatus.png
	:align: center

After pressing the **Run Query** button, the status frame will show information
about the progress of a query.

You can pause a query at any time and you can cancel a query, as well; nevertheless
you need to wait for the actual page query being ready before pause or cancelation
takes place.

