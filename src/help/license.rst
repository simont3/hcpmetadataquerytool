*******
License
*******


HCP Metadata Query Tool is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HCP Metadata Query Tool is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with HCP Metadata Query Tool. If not, see `the license page at gnu.org`_.

Copyright 2012-2015 `Thorsten Simons`_



.. _`Thorsten Simons`: mailto:sw@snomis.de
.. _`the license page at gnu.org`: http://www.gnu.org/licenses/