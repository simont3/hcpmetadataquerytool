*********************
HCP access Parameters
*********************

.. image:: images/hcpaccess.png
	:align: center

In this fields, you specify the parameter needed to access HCP and your area of
interest within.

.. TIP::
	Depending on the access rights you have for HCP, use different names:
	
	*	enter ``admin.hcp.your-domain.com`` if you have a *HCP System Console* account
		with the Search Role enabled
	*	enter ``tenant.hcp.your-domain.com`` if you have a *HCP Tenant Console* account
		for the specified Tenant with the Search Role enabled
	*	enter ``tenant.hcp.your-domain.com`` **and** ``namespace.tenant`` if you have
		a *Data Access* account	for the specified Namespace with the Search Role enabled 

You may further restrict the result by defining folders that should be queried - this will
skip any other folders.
	
