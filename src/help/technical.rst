*********************
Technical description
*********************

This is what happens when you hit the **Run Query** button:

*	The Domain Name Server is queried for the IP addresses of *HCP*. The first address
	received is the one that will be used for all communication with HCP during this query. 

*	The parameters given are used to build a query in XML format,

*	which is then send to *HCP*.

*	HCP runs an internal query against its database and delivers the first page of results
	back to *HCPmqt*.

*	*HCPmqt* processes the page and -if the page isn't flagged as *COMPLETED*- will build the
	next query-XML.

This runs in a loop until all requested records have been received.

