**********
Time Range
**********

.. image:: images/hcptimerange.png
	:align: center

You can specify a time range for the query.
	
Per default, values are provided for a *full query*, which means anything from
Jan. 1st, 1970 until **now** (use the *Reset* button to reset the fields).
	
.. TIP::
	Normally, you need to enter a timestamp exactly in the given format. In addition to this,
	a number of seconds counted from Jan. 1st 1970 (the Unix-epoch) will be accepted, also.