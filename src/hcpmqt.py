#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>

import sys, re

from PyQt4 import QtGui
from hcpmqt.hcpmqtgui import MyMainWindow


def main(argv):
    app = QtGui.QApplication(argv)
    
    """
    # Setup translation service
    language = QtCore.QLocale.system().name()
    # a.) system
    qtTranslator = QtCore.QTranslator()
    qtTranslator.load('qt_{0}'.format(language),
                      QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath))
    app.installTranslator(qtTranslator)
    # b.) hcpmqt specifics
    myappTranslator = QtCore.QTranslator()
    myappTranslator.load(':/translations/hcpmqt_{0}'.format(language))
    app.installTranslator(myappTranslator)
    """
    
    mainwindow = MyMainWindow()
    mainwindow.show()
    
    sys.exit(app.exec_())
    

if __name__ == '__main__':
    main(sys.argv)
    