#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of HCP Metadata Query Tool.
#
# HCP Metadata Query Tool is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HCP Metadata Query Tool is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2012-2015 Thorsten Simons <mailto:sw@snomis.de>

import os, sys
from cx_Freeze import setup, Executable
from hcpmqt.hcpmqtinit import gVars

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"
imgFmtPth = os.path.join(os.path.dirname(sys.executable),
                         'Lib/site-packages/PyQt4/plugins/imageformats')

buildOptions = {'compressed': True,
                'include_msvcr': True,
                'icon': "..\\data\\resources\\HCPmqt_32x32.ico",
                'include_files': [(imgFmtPth, ''),
                                  ('c:\windows\system32\msvcp100.dll', '')],
                }
descriptionText = "Version {0}".format(gVars._version)

setup(
      version=gVars._version,
      name = "HCP Metadata Query Tool",
      author='Thorsten Simons',
      author_email='sw@snomis.de',
      url='https://sourceforge.net/projects/hcpmetadataquer/',
      platforms=['Windows'],
      description = descriptionText,
      options = dict(build_exe = buildOptions),
      executables = [Executable("hcpmqt.py", base=base)]
      )