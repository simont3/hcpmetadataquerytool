# -*- coding: utf-8 -*-
'''
This file is part of HCP Metadata Query Tool.

Copyright 2012 Thorsten Simons <mailto:sw@snomis.de>

HCP Metadata Query Tool is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HCP Metadata Query Tool is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with HCP Metadata Query Tool. If not, see <http://www.gnu.org/licenses/>.
'''

# Convert Qt-Designer files into Python source

import sys, os
from subprocess import call

if sys.platform == 'win32':
    bindir = os.path.join(os.path.dirname(sys.executable),
                          'Lib\\site-packages\\PyQt4')
else:
    bindir = '/usr/bin'
    
uic_path = os.path.join(bindir, 'pyuic4.bat')
rcc_path = os.path.join(bindir, 'pyrcc4.exe')

ui_path = 'ui'
rc_path = ''

out_path = os.path.normpath('src/hcpmqt/ui')

ui_files = {'hcpmqt.ui': 'ui_hcpmqt.py',
            'hcpmqthelp.ui': 'ui_hcpmqthelp.py'}
rc_files = {'hcpmqt.qrc': 'hcpmqt_rc.py'}

for file in ui_files:
    file_path = os.path.normpath(os.path.join(ui_path, file))
    out_file_path = os.path.normpath(os.path.join(out_path, ui_files[file]))
    print('uic_path =', uic_path)
    print('file_path =', file_path)
    print('out_file_path =', out_file_path)
    call([uic_path, file_path, '--from-imports', '-o', out_file_path])

for file in rc_files:
    file_path = os.path.join(rc_path, file)
    out_file_path = os.path.join(out_path, rc_files[file])
    call([rcc_path, file_path, '-py3', '-o', out_file_path])
    