HCP Metadata Query Tool
=======================

HCP Metadata Query Tool enables you to query an Hitachi Content Platform
(HCP) based on operations on objects happened in the past.
It facilitates the Metadata Query Engine integrated in HCP to list
object metadata for all or a subset of the data stored in HCP.

You need to have HCP integrated into your DNS environment (means,
you can't use it with an HCP node's ip-address only).

You need to have an user on HCP which has the Search-role enabled.
Depending on the user, you will have access to different areas of HCP:

- administrative users with access to the System Console will be allowed
  to query all Tenants (and it's Namespaces) where a Tenant
  administrator has granted access to System Console administrators
  ('Allow HCP system-level users to manage me and search across my space'
  on the Tenant's overview panel)

- administrative Tenant users will be allowed to query all Namespaces
  within his Tenant

- Data Access users will be allowed to query all Namespaces where hes
  has been granted the Search role


There's **no need** to enable MAPI access to use HCP Metadata Query Tool.


Prerequisites
-------------

======================  ==================================================
Software                Comment
======================  ==================================================
Python 3.3 or better    installed in single user mode, due to placement of
					    msvcr100.dll
					    (http://python.org)
PyQt v4                 for Python v3.3 or better
dnspython 1.11.1	    used to get hold of the ip address of the target
					    node withon the HCP cluster
					    (http://www.dnspython.org/)
Sphinx 1.2b1		    used to build the help pages
					    (http://sphinx-doc.org)
cx_freeze 4.3.2		    used to freeze the python code into an executable
					    (http://cx-freeze.sourceforge.net/)
Inno Setup 5.5		    used to build a Windows installer
					    (http://www.jrsoftware.org/isinfo.php)
======================  ==================================================


Installation
------------

Get the last Windows executable from
`Github <https://gitlab.com/simont3/hcpmetadataquerytool/tree/master/Executables>`_

-or-

make sure you have all the pre-requisites installed, then

* get the source from `GitHub <https://github.com/Simont3/hcpmetadataquerytool/archive/master.zip>`_
* unzip the archive
* run ``python setup.py bdist``
* run Inno Setup

or

* Fork at `Github <https://github.com/Simont3/hcpmetadataquerytool>`_

Contribute
----------

- Issue Tracker: `<https://github.com/Simont3/hcpmetadataquerytool/issues>`_
- Source Code: `<https://github.com/Simont3/hcpmetadataquerytool>`_

Support
-------

If you are having issues, please let us know via the Issue Tracker
or send an email to `<sw@snomis.de>`_

License
-------

The MIT License (MIT)

Copyright (c) 2014-2015 Thorsten Simons (sw@snomis.de)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
